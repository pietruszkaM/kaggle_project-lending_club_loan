# README #

This is a finished project in which I have used many different Machine Learning and Statistical Learning techniques. 
In this project, I have used different Classification models. I have created many additional features, that improves performance.

### What is this repository for? ###

* This repository contains files that I have created during the project. There is Python code used to solve the task and pdf version of the report.
* The project was initiated by myself and done in my free time.
* Version 1.0

### What to look for? ###

* Objective was to get more experience in building different models and not to use a huge number of estimators inside each one.
* Dataset is taken from https://www.kaggle.com/wendykan/lending-club-loan-data
* Python 3.6
* Libraries used: pandas, matplotlib, numpy, math, seaborn, scipy, string, sklearn
* Metric: ROC AUC

### Models built ###

* Classifier Models: MultiLayerPerceptron, DecisionTree, AdaBoost, GradientBoosting, RandomForrest, KNearestNeighbors, Dummy, Bagging, ExtraTrees, Stochastic Gradient Descent, VotingClassifier
* Regression Models - LogisticRegression

### Contact me ###

* email: pietruszka@agh.edu.pl
* LinkedIn: https://www.linkedin.com/in/pietruszka-phd/
* BitBucket: https://bitbucket.org/pietruszkaM/

### Thank you for watching! ###